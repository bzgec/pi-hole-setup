#!/bin/bash

# https://www.reddit.com/r/pihole/comments/rmb6xn/autoupdate_pihole_and_gravity_list/

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

echo '############################################################'
date +'%m/%d/%Y %H:%M:%S'
uptime -p

# Update Pi-Hole
echo '########## "pihole -up"'
/usr/bin/sudo pihole -up

# Do apt-get update
echo '########## "apt update --fix-missing"'
/usr/bin/sudo apt update --fix-missing

# Then the upgrade
echo '########## "apt -y upgrade"'
/usr/bin/sudo apt -y upgrade

# Reboot
echo '########## "systemctl reboot -i"'
/usr/bin/sudo systemctl reboot -i
