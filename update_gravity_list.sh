#!/bin/bash

# https://www.reddit.com/r/pihole/comments/rmb6xn/autoupdate_pihole_and_gravity_list/

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

echo '############################################################'
date +'%m/%d/%Y %H:%M:%S'
uptime -p

# Update Pi-Hole gravity list
echo '########## "pihole -g"'
/usr/bin/sudo pihole -g
