#!/bin/bash

set -euxo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

# Update and upgrade the system
yes | apt update
yes | apt upgrade

# Install needed programs
yes | apt install vim ufw fail2ban

####################################################################################################
# Setup firewall - ufw
####################################################################################################
# https://wiki.archlinux.org/title/Uncomplicated_Firewall

ufw reset  # Clear any previous configuration
ufw default deny incoming  # Block all incoming traffic
ufw allow ssh
ufw limit ssh

# Pi-hole
# https://docs.pi-hole.net/main/prerequisites/
ufw allow 80/tcp
ufw allow 53/tcp
ufw allow 53/udp

# DHCP
ufw allow 67/udp
ufw allow 547/udp

#ufw allow 4711/tcp

ufw enable

echo "Starting and enabling ufw"
systemctl enable ufw.service
systemctl start ufw.service
####################################################################################################

####################################################################################################
# Setup fail2ban
####################################################################################################
echo "Setup jail for naughty SSH attempts"
cat <<EOT > /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled   = true
filter    = sshd
banaction = ufw
backend   = systemd
maxretry  = 5
findtime  = 1d
bantime   = 52w
EOT

echo "Starting and enabling the jail/fail2ban"
systemctl enable fail2ban.service
systemctl start fail2ban.service
####################################################################################################

####################################################################################################
# Setup pi-hole
####################################################################################################
# https://github.com/pi-hole/pi-hole
echo "Setup pi-hole"
curl -sSL https://install.pi-hole.net | bash
####################################################################################################

echo 'Script finished'
