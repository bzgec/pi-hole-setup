=============
Pi-hole setup
=============

.. role:: bash(code)
   :language: bash

Tested with `Armbian Bullseye Minimal CLI <https://www.armbian.com/orangepi3-lts/>`__
on Orange Pi 3 LTS.

Setup
=====

Armbian
-------

1. Launch `setup_armbian.sh <setup_armbian.sh>` as a superuser: :bash:`sudo bash setup_armbian.sh`

2. Run updates every week, add this to the superuser crontab:

   1. :bash:`sudo crontab -e`

   2. Add:

      .. code-block:: bash

         \# At 01:00.
         0 1 * * * /usr/bin/bash /home/USER/pi-hole-setup/update_gravity_list.sh >> /home/USER/pi-hole-setup/update_gravity_list.log 2>&1

         \# At 02:00 on Monday.
         0 2 * * 1 /usr/bin/bash /home/USER/pi-hole-setup/update_armbian.sh >> /home/USER/pi-hole-setup/update_armbian.log 2>&1

3. Setup static IP

   Add this to `/etc/network/interfaces`.

   .. code-block:: bash

      auto eth0
      allow-hotplug eth0
      #no-auto-down eth0
      iface eth0 inet static
      address 192.168.0.11
      netmask 255.255.255.0
      gateway 192.168.0.1

   .. note::

      You probably need to change *address* and *gateway* or even *eth0*.

4. Disable ssh password authentication

   1. Generate private and public RSA key pair: :bash:`ssh-keygen -b 4096`
   2. Select name that is not the same as default if you want to use multiple/different keys.
   3. Create also a password
   4. Key pair should now be generated, copy it to ``~/.ssh/`` folder
   5. Run this command to copy public RSA key to client/remote host:
      :bash:`cat ~/.ssh/RSA_KEY.pub | ssh USERNAME@REMOTE_HOST "mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh && cat >> ~/.ssh/authorized_keys"`
   6. You can now login to remote host with :bash:`ssh USERNAME@REMOTE_HOST -i ~/.ssh/RSA_KEY`
   7. Disabling Password Authentication on remote host:

      Edit `/etc/ssh/sshd_config` and set *PasswordAuthentication* to *no*: :bash:`PasswordAuthentication no`

   8. Restart ssh service: :bash:`systemctl restart sshd.service`

   - Note that if you use the default RSA key there is no need to pass ``-i RSA_KEY``
   - `Reference <https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04>`__

5. To move system to eMMC run :bash:`sudo nand-sata-install` and select *Boot from eMMC - system on eMMC*

6. Turn OFF green LED 10 minutes after boot

   1. :bash:`sudo crontab -e`

   2. Add:

      .. code-block:: bash

         # Turn OFF the green LED (status LED) 10 minutes after boot.
         # It needs to be turned ON after boot because it will automatically return to its previous state.
         @reboot echo 1 > /sys/devices/platform/leds/leds/green-led/brightness
         @reboot sleep 600 && echo 0 > /sys/devices/platform/leds/leds/green-led/brightness



_
